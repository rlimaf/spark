from pyspark.sql import SparkSession
from json import dumps, loads
import firebase_admin
from firebase_admin import credentials, storage

spark = SparkSession.builder.master('local[2]')\
  .appName('Desemprego')\
  .config("spark.some.config.option", "some-value")\
  .getOrCreate()

df = spark.read.csv('./data/desemprego.csv', header=True, encoding='ISO-8859-1', sep=';')
size = df.count()

df1 = df.select('Sexo', 'd_Situação', 'd_faixa_Idade')
ndf = df1.rdd.flatMap(lambda x: [((x['Sexo'], x['d_Situação'], x['d_faixa_Idade']), 1)])\
  .reduceByKey(lambda x, y: x + y)\
  .flatMap(lambda x: [(x[0][0], x[0][1], x[0][2], x[1], round((x[1]*100)/size, 2))])\
  .toDF(['gender', 'situaction', 'age_range', 'total', 'percent'])\
  .orderBy('gender', 'situaction', 'age_range')

json = ndf.toPandas().to_json(orient='records')
try:
  app = firebase_admin.get_app()
except ValueError as e:
  cred = credentials.Certificate('credentials/firebase-config.json')
  firebase_admin.initialize_app(cred, {'storageBucket': 'big-data-38723.appspot.com'})
with open('output/data.json', 'w') as f:
  f.write(dumps(loads(json), indent=2, ensure_ascii=False))
f.close()
fileName = 'output/data.json'
bucket = storage.bucket()
blob = bucket.blob(fileName)
blob.upload_from_filename(fileName)
blob.make_public()
